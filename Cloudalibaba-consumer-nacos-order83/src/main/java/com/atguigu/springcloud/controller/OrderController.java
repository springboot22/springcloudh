package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.entity.CommenResult;
import com.atguigu.springcloud.entity.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @author will.tuo
 * @date 2021/9/2 11:05
 */
@RestController
@Slf4j
public class OrderController {

    @Autowired
    RestTemplate restTemplate;

    @Value("${server-url.nacos-user-service}")
    private  String PAYMENT_URL;

    @RequestMapping("/comsumer/payment/nacos/{id}")
    public String create(@PathVariable("id") Integer id){
        return restTemplate.getForObject(PAYMENT_URL+"/payment/nacos/"+id, String.class);
    }

    @RequestMapping("/comsumer/select/{id}")
    public CommenResult<Payment> create(@PathVariable long id){
        return restTemplate.getForObject(PAYMENT_URL+"/payment/select/"+id,CommenResult.class,id);
    }
}
