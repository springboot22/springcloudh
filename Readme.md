# SpringCloud Alibaba 学习

Springboot+ RestTemplate + Nacos (+ Eureka 后期用的是Nacos)





## 模块列表



## 文件上传FileResourceUploadService

### Nginx文件浏览器

配置访问路径

```conf
 location /www {
            alias       /myfile/;
            autoindex_exact_size off;
            autoindex_localtime on;
            autoindex   on;
            charset     utf-8,gbk;
        }
```

1、使用root配置路径时会访问不到所以改为alias。

2、charset 不配置的话，会出现文件名显示中文乱码的问题。

结果展示

![image-20211008100112645](img/image-20211008100112645.png)

### 文件上传模块

后端模块借用了

https://github.com/gaoyuyue/MyUploader-Backend

1、在此基础上，添加一个修改文件名的功能，因为原本的文件上传时在进行过程中会有一个临时的文件名，但当文件上传完毕后，不会将文件名改回他原本的名字。

2、文件上传路径也是直接配置在Nacos中

前端直接用了该老兄的前端代码

https://github.com/gaoyuyue/MyUploader