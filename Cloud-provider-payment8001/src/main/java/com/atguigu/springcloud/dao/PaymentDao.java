package com.atguigu.springcloud.dao;

import com.atguigu.springcloud.entity.Payment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author will.tuo
 * @date 2021/9/2 10:55
 */
@Mapper
public interface PaymentDao extends BaseMapper<Payment> {
}
