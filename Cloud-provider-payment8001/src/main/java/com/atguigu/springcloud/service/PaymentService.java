package com.atguigu.springcloud.service;


import com.atguigu.springcloud.entity.Payment;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/9/2 11:00
 */
@Service
public interface PaymentService extends IService<Payment> {
}
