package com.atguigu.springcloud.controller;


import com.atguigu.springcloud.entity.CommenResult;
import com.atguigu.springcloud.entity.Payment;
import com.atguigu.springcloud.service.Impl.PaymentServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author will.tuo
 * @date 2021/9/2 11:05
 */
@RestController
@Slf4j
public class PaymentController {
    @Autowired
    PaymentServiceImpl paymentService;

    @RequestMapping("/payment/create")
    public CommenResult create(@RequestBody Payment payment){
        boolean result = paymentService.save(payment);
        return new CommenResult(result==true?200:400,result?"s":"f");
    }
    @GetMapping("/payment/select/{id}")
    public CommenResult selectById(@PathVariable long id){
        Payment payment = paymentService.getById(id);
        return new CommenResult(payment==null?400:200,payment!=null?"s":"f",payment);
    }
}
