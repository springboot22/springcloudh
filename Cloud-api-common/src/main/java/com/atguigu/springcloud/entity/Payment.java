package com.atguigu.springcloud.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author will.tuo
 * @date 2021/9/2 10:29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("payment")
public class Payment {
    @TableId(value = "id",type = IdType.AUTO)
    private long id;
    private String serial;
}
