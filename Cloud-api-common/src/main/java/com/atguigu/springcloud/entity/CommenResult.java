package com.atguigu.springcloud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author will.tuo
 * @date 2021/9/2 10:51
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommenResult<T> {
    private Integer code;
    private String message;
    private T data;
    public CommenResult(Integer code, String message){
        this.code = code;
        this.message = message;
        this.data = null;
    }
}
