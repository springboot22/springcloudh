package myuploader.service;

import static myuploader.utils.UploadUtils.addChunk;
import static myuploader.utils.UploadUtils.getFileName;
import static myuploader.utils.UploadUtils.isUploaded;
import static myuploader.utils.UploadUtils.removeKey;

import java.io.IOException;
import java.util.Date;
import myuploader.config.UploadConfig;
import myuploader.dao.FileDao;
import myuploader.model.File;
import myuploader.utils.FileUtils;
import myuploader.utils.FileUtils2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件上传服务
 */
@Service
public class FileService {

    @Autowired
    private FileDao fileDao;


    /**
     * 上传文件
     *
     * @param md5
     * @param file
     */
    public void upload(String name,
            String md5,
            MultipartFile file) throws IOException {
        String path = UploadConfig.path + name;
        FileUtils.write(path, file.getInputStream());
        fileDao.save(new File(name, md5, path, new Date()));
    }

    /**
     * 分块上传文件
     *
     * @param md5
     * @param size
     * @param chunks
     * @param chunk
     * @param file
     * @throws IOException
     */
    public void uploadWithBlock(String name,
            String md5,
            Long size,
            Integer chunks,
            Integer chunk,
            MultipartFile file) throws Exception {
        String fileName = getFileName(md5, chunks);
        FileUtils.writeWithBlok(UploadConfig.path + fileName, size, file.getInputStream(), file.getSize(), chunks,
                chunk);
        addChunk(md5, chunk);
        if (isUploaded(md5)) {
            removeKey(md5);
            //修改文件名你 从随机文件名到真实文件名
//            FileUtils2.renameFile(UploadConfig.path + fileName, name);
            FileUtils2.copyFile(UploadConfig.path + fileName, UploadConfig.path + name);
            FileUtils2.deleteFile(UploadConfig.path + fileName);
            fileDao.save(new File(name, md5, UploadConfig.path + fileName, new Date()));
        }
    }

    /**
     * 检查Md5判断文件是否已上传
     *
     * @param md5
     * @return
     */
    public boolean checkMd5(String md5) {
        File file = new File();
        file.setMd5(md5);
        return fileDao.getByFile(file) == null;
    }
}
