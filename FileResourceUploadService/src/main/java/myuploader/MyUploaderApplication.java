package myuploader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class MyUploaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyUploaderApplication.class, args);
    }
}
