package myuploader.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author will.tuo
 * @date 2021/9/27 15:38
 */
@RestController
@RequestMapping("/nacos")
public class NacosTestController {

    @Value("${my.info}")
    private String myinfo;

    @RequestMapping("/get")
    public String get() {
        return myinfo + "jko";
    }
}
