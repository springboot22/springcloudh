package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.entity.CommenResult;
import com.atguigu.springcloud.entity.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @author will.tuo
 * @date 2021/9/2 11:05
 */
@RestController
@Slf4j
public class OrderController {
    @Autowired
    RestTemplate restTemplate;
    private static final String PAYMENT_URL = "http://localhost:8081";
    @RequestMapping("/comsumer/create")
    public CommenResult<Payment> create(@RequestBody Payment payment){
        return restTemplate.postForObject(PAYMENT_URL+"/payment/create",payment, CommenResult.class);
    }

    @RequestMapping("/comsumer/select/{id}")
    public CommenResult<Payment> create(@PathVariable long id){
        return restTemplate.getForObject(PAYMENT_URL+"/payment/select/"+id,CommenResult.class,id);
    }
}
