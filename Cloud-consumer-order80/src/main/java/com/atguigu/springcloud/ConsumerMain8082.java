package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author will.tuo
 * @date 2021/9/2 13:12
 */
@SpringBootApplication
@EnableEurekaClient
public class ConsumerMain8082 {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerMain8082.class,args);
    }
}
