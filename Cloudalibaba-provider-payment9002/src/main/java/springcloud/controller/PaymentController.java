package springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author will.tuo
 * @date 2021/9/22 14:03
 */
@RestController
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;
    @Value("${cxzv.dvsd}")
    private String cd;

    @GetMapping("/payment/nacos/{id}")
    public String getPayment(@PathVariable("id") Integer id) {
        return "nacos registry, serverPort:" + serverPort + "\t id:" + id;
    }
}
