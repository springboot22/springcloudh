package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author will.tuo
 * @date 2021/9/22 14:00
 */
@SpringBootApplication
@EnableDiscoveryClient
public class NacosPaymentProvider {

    public static void main(String[] args) {
        SpringApplication.run(NacosPaymentProvider.class,args);
    }
}
